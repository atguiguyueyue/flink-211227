package com.atguigu.day11;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.SqlDialect;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.catalog.hive.HiveCatalog;

public class Flink04_Catalog_Hive {
    public static void main(String[] args) {
        //设置用户权限
        System.setProperty("HADOOP_USER_NAME", "atguigu");

        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //2.获取表的执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //TODO 3. 创建Hivecatalog

        String name = "hivecatalog";
        String dafaultDatabases = "flink_test";
        String hiveConfDir = "c:/conf";

        HiveCatalog hiveCatalog = new HiveCatalog(name, dafaultDatabases, hiveConfDir);

        //TODO 4.注册catalog
        tableEnv.registerCatalog(name, hiveCatalog);

        //TODO 5.指定使用哪个catalog
        tableEnv.useCatalog(name);

        //TODO 6.指定使用哪个数据库
        tableEnv.useDatabase("flink_test");

        //设置hive方言
        tableEnv.getConfig().setSqlDialect(SqlDialect.HIVE);

        //TODO 7.写sql操作hive中的数据
        tableEnv.executeSql("select * from stu").print();
    }
}
