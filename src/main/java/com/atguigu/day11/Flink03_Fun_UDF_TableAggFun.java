package com.atguigu.day11;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.functions.AggregateFunction;
import org.apache.flink.table.functions.TableAggregateFunction;
import org.apache.flink.util.Collector;

import static org.apache.flink.table.api.Expressions.$;
import static org.apache.flink.table.api.Expressions.call;

public class Flink03_Fun_UDF_TableAggFun {
    public static void main(String[] args) throws Exception {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //2.从文件读取数据
        SingleOutputStreamOperator<WaterSensor> waterSensorDStream = env
                .socketTextStream("localhost", 9999)
                .map(new MapFunction<String, WaterSensor>() {
                    @Override
                    public WaterSensor map(String value) throws Exception {
                        String[] split = value.split(",");
                        return new WaterSensor(split[0], Long.parseLong(split[1]), Integer.parseInt(split[2]));
                    }
                });

        //3.获取表的执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //4.将流转为表
        Table table = tableEnv.fromDataStream(waterSensorDStream);

        //TODO 不注册直接使用自定义函数
//        table
//                .groupBy($("id"))
//                .flatAggregate(call(MyUDTAF.class,$("vc")))
//                .select($("id"),$("f0"),$("f1"))
//                .execute().print();

        //TODO 先注册再使用
        tableEnv.createTemporarySystemFunction("myTop2", MyUDTAF.class);

        table
                .groupBy($("id"))
                .flatAggregate(call("myTop2",$("vc")).as("value", "rank"))
                .select($("id"),$("value"),$("rank"))
                .execute().print();


    }

    //自定义一个表聚合函数（多进多出）根据id求vc的最大两个值
    public static class MyTopAcc{
        public Integer first = Integer.MIN_VALUE;
        public Integer second = Integer.MIN_VALUE;
    }
    public static class MyUDTAF extends TableAggregateFunction<Tuple2<Integer,String>,MyTopAcc>{

        @Override
        public MyTopAcc createAccumulator() {
            return new MyTopAcc();
        }

        public void accumulate(MyTopAcc acc,Integer value){
            if (value>acc.first){
                acc.second = acc.first;
                acc.first = value;
            }else if (value>acc.second){
                acc.second = value;
            }
        }

        public void emitValue(MyTopAcc acc, Collector<Tuple2<Integer,String>> out){
            if (acc.first!=Integer.MIN_VALUE){

                out.collect(Tuple2.of(acc.first,"1"));
            }

            if (acc.second!=Integer.MIN_VALUE){

                out.collect(Tuple2.of(acc.second,"2"));
            }
        }

//        public void merge(MyTopAcc acc,Iterable<MyTopAcc> iter){
//
//        }

    }
}
