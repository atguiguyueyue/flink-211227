package com.atguigu.day03;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;
import java.util.List;

public class Flink01_Source_Collection {
    public static void main(String[] args) throws Exception {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //TODO 2.从集合中获取数据
        //创建一个集合
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);

//        DataStreamSource<Integer> streamSource = env.fromCollection(list);

        //TODO 3.从元素中获取数据
        DataStreamSource<String> streamSource = env.fromElements("a", "b", "c", "d").setParallelism(2);

        streamSource.print();

        env.execute();
    }
}
