package com.atguigu.day03;

import org.apache.flink.streaming.api.datastream.ConnectedStreams;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoMapFunction;

public class Flink10_Transform_connect {
    public static void main(String[] args) throws Exception {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //2.获取字母流
        DataStreamSource<String> strStreamSource = env.fromElements("a", "b", "c", "d", "e");

        //数字流
        DataStreamSource<Integer> intStreamSource = env.fromElements(1, 2, 3, 4, 5);
        //TODO 3.使用connect连接两条流
        ConnectedStreams<String, Integer> connect = strStreamSource.connect(intStreamSource);

        //4.对连接后的流做map操作
        SingleOutputStreamOperator<String> map = connect.map(new CoMapFunction<String, Integer, String>() {
            @Override
            public String map1(String value) throws Exception {
                return value + "aaa";
            }

            @Override
            public String map2(Integer value) throws Exception {
                return value * value + "";
            }
        });

        map.print();

        env.execute();

    }
}
