package com.atguigu.day03;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.ConnectedStreams;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.co.CoMapFunction;

public class Flink11_Transform_union {
    public static void main(String[] args) throws Exception {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //2.获取字母流
        DataStreamSource<String> strStreamSource = env.fromElements("a", "b", "c", "d", "e");

        //数字流
        DataStreamSource<String> numStreamSource = env.fromElements("1", "2", "3", "4", "5");

        //TODO 3.使用union连接多条流
        DataStream<String> union = strStreamSource.union(numStreamSource);


        //4.对数据做map操作
        union.map(new MapFunction<String, String>() {
            @Override
            public String map(String value) throws Exception {
                return value + "zzzz";
            }
        }).print();

        env.execute();

    }
}
