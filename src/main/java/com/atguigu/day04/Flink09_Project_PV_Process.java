package com.atguigu.day04;

import com.atguigu.bean.UserBehavior;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;

import java.util.stream.Stream;

public class Flink09_Project_PV_Process {
    public static void main(String[] args) throws Exception {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //2.从文件读取数据然后使用process处理
        env
                .readTextFile("input/UserBehavior.csv")
                .process(new ProcessFunction<String, Tuple2<String, Integer>>() {
                    //定义一个累加器
                    private Integer count = 0;

                    @Override
                    public void processElement(String value, Context ctx, Collector<Tuple2<String, Integer>> out) throws Exception {
                        //1.切分数据获取到每一列数据
                        String[] split = value.split(",");

                        //2.将一行数据组成JavaBean
                        UserBehavior userBehavior = new UserBehavior(Long.parseLong(split[0]),
                                Long.parseLong(split[1]),
                                Integer.parseInt(split[2]),
                                split[3],
                                Long.parseLong(split[4]));

                        //3.过滤出pv的数据
                        if ("pv".equals(userBehavior.getBehavior())) {
                            count++;
                            out.collect(Tuple2.of("pv", count));
                        }

                    }
                }).print();

        env.execute();
    }
}
