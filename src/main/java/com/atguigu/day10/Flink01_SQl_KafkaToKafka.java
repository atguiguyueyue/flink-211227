package com.atguigu.day10;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Flink01_SQl_KafkaToKafka {
    public static void main(String[] args) {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //2.获取表的执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //TODO 3.创建一张表，消费kafka数据
        tableEnv.executeSql("create table source_sensor (id string, ts bigint, vc int) with(" +
                "'connector' = 'kafka'," +
                "'topic' = 'topic_source_sensor'," +
                "'properties.bootstrap.servers' = 'hadoop102:9092'," +
                "'properties.group.id' = '1227'," +
                "'scan.startup.mode' = 'latest-offset'," +
                "'format' = 'csv'" +
                ")");

        //TODO 4.创建一张表，往kafka写数据
        tableEnv.executeSql("create table sink_sensor(id string, ts bigint, vc int) with("
                + "'connector' = 'kafka',"
                + "'topic' = 'topic_sink_sensor',"
                + "'properties.bootstrap.servers' = 'hadoop102:9092',"
                + "'format' = 'csv'"
                + ")"
        );

        //TODO 5.将查询source_sensor这个表的数据插入到sink_sensor，从而实现kafka的数据从一个topic到另一个topic
        tableEnv.executeSql("insert into sink_sensor select * from source_sensor");
    }
}
