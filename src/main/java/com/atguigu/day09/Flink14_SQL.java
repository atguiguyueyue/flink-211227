package com.atguigu.day09;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Flink14_SQL {
    public static void main(String[] args) {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setParallelism(1);

        //2.获取表的执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        DataStreamSource<WaterSensor> waterSensorStream =
                env.fromElements(new WaterSensor("sensor_1", 1000L, 10),
                        new WaterSensor("sensor_1", 2000L, 20),
                        new WaterSensor("sensor_2", 3000L, 30),
                        new WaterSensor("sensor_1", 4000L, 40),
                        new WaterSensor("sensor_1", 5000L, 50),
                        new WaterSensor("sensor_2", 6000L, 60));

        //TODO 3.查询未注册的表
//        Table table = tableEnv.fromDataStream(waterSensorStream);

//        tableEnv.executeSql("select * from "+table+" where id ='sensor_1'").print();

        //TODO 查询已注册的表(有表名)  利用Table对象注册
//        tableEnv.createTemporaryView("sensor", table);

        //TODO 查询已注册的表 利用流注册
        tableEnv.createTemporaryView("sensor", waterSensorStream);

        tableEnv.executeSql("select * from sensor where id = 'sensor_1'").print();
    }
}
