package com.atguigu.day09;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.DataTypes;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;
import org.apache.flink.table.descriptors.Csv;
import org.apache.flink.table.descriptors.FileSystem;
import org.apache.flink.table.descriptors.Schema;
import org.apache.flink.types.Row;

import static org.apache.flink.table.api.Expressions.$;

public class Flink10_TableAPI_Connect_File {
    public static void main(String[] args) throws Exception {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //2.获取表的执行环境
        StreamTableEnvironment tableEnv = StreamTableEnvironment.create(env);

        //TODO 3.连接外部文件系统，读取数据，并映射为动态表
        Schema schema = new Schema();
        schema.field("id", DataTypes.STRING());
        schema.field("ts", DataTypes.BIGINT());
        schema.field("vc", DataTypes.INT());
        tableEnv.connect(new FileSystem().path("input/sensor-sql.txt"))
                .withFormat(new Csv().lineDelimiter("\n").fieldDelimiter(','))
                .withSchema(schema)
                .createTemporaryTable("sensor");

        //4.将表转为Table对象
        Table table = tableEnv.from("sensor");
//        Table resultTable = table.where($("id").isEqual("sensor_1"))
//                .select($("id"), $("ts"), $("vc"));

        //将Table对象转为TableResult对象可以直接打印
//        resultTable.execute().print();

        //使用sql查询数据,不需要将path转为Table对象，使用sqlQuery方法返回的是Table对象，然后可以调用Execute转为TableResult对象对其打印
//        Table sqlQuery = tableEnv.sqlQuery("select * from sensor where id = 'sensor_1' ");
//        sqlQuery.execute().print();

        //使用sql查询数据，直接调用executeSql方法，返回的是一个TableResult对象，可以直接对其打印
//        tableEnv.executeSql("select * from sensor where id = 'sensor_1' ").print();

        //如果只有Table对象，没有path，则可以以一下方式写sql
        tableEnv.executeSql("select * from "+table+ " where id = 'sensor_1'").print();

        //5.将表转为流
//        tableEnv.toAppendStream(resultTable, Row.class).print();

//        env.execute();
    }
}
