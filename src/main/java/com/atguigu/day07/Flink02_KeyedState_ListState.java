package com.atguigu.day07;

import com.atguigu.bean.WaterSensor;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Comparator;

public class Flink02_KeyedState_ListState {
    public static void main(String[] args) throws Exception {
        //1.获取流的执行环境
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        //2.从端口获取数据，并转为JavaBean-》将相同的id聚合到一块
        KeyedStream<WaterSensor, Tuple> keyedStream = env
                .socketTextStream("localhost", 9999)
                .map(new MapFunction<String, WaterSensor>() {
                    @Override
                    public WaterSensor map(String value) throws Exception {
                        String[] split = value.split(",");
                        return new WaterSensor(split[0], Long.parseLong(split[1]), Integer.parseInt(split[2]));
                    }
                })
                .keyBy("id");


        //3.针对每个传感器输出最高的3个水位值
        keyedStream.process(new KeyedProcessFunction<Tuple, WaterSensor, String>() {

            //TODO 1.定义一个状态保存最高的三个水位值
            private ListState<Integer> listState;


            //TODO 2.初始化状态
            @Override
            public void open(Configuration parameters) throws Exception {
                listState = getRuntimeContext().getListState(new ListStateDescriptor<Integer>("list-state", Integer.class));
            }

            @Override
            public void processElement(WaterSensor value, Context ctx, Collector<String> out) throws Exception {
                //1.将当前的水位保存到状态中
                listState.add(value.getVc());

                //2.将状态中的值取出来
                Iterable<Integer> iterable = listState.get();

                //3.创建一个list集合用来保存迭代器中的数据
                ArrayList<Integer> listVc = new ArrayList<>();
                for (Integer integer : iterable) {
                    listVc.add(integer);
                }

                //4.判断集合中的元素个数是否大于三，如果大于三的话则排序取前三个
                if (listVc.size()>3){
                    listVc.sort(new Comparator<Integer>() {
                        @Override
                        public int compare(Integer o1, Integer o2) {
                            return o2-o1;
                        }
                    });

                    //5.删除经过排序后的最后一个元素（最小的）
                    listVc.remove(3);
                }

                //6.将list集合中的数据更新到状态中
                listState.update(listVc);

                out.collect(listVc.toString());
            }
        }).print();

        env.execute();
    }
}
